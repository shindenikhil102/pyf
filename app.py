import json
from flask import Flask, request
import datetime as dt
import uuid

from server_files.FetchKiteData import FetchKiteData
from server_files.SqlConnector import SqlConnector
from server_files.Logger import Logger

app = Flask(__name__)


@app.route('/')
def hello():
    return 'Hello World!'


@app.route('/webhook/<strategy_name>', methods=['POST'])
def webhook(strategy_name):
    data = json.loads(request.data)
    strategy = strategy_name
    raw_data = data['raw_data']
    alert_time = dt.datetime.strptime(data['alert_time'], '%d-%m-%Y %H:%M:%S')

    mysql_time_stamp = dt.datetime.now()
    date = alert_time.date()
    time_hour = alert_time.hour
    time_min = alert_time.minute
    time_seconds = alert_time.second

    # Add data to sql here
    row_id = sql_obj.add_raw_alert(mysql_time_stamp, str(
        raw_data), alert_time, date, time_hour, time_min, time_seconds)
    # read raw data
    script_name = raw_data['script_name']
    contract = dt.datetime.strptime(raw_data['contract'], '%y%b')
    close = raw_data['current_price']
    openx = raw_data['current_candle_open']
    low = raw_data['current_candle_low']
    high = raw_data['current_candle_high']
    volume = raw_data['current_candle_volume']
    strat = raw_data['strategy_name']
    strategy_timeframe_number = raw_data['str_timeframe_number']
    strategy_timeframe_unit = raw_data['str_timeframe_unit']
    action_first = raw_data['action_first']
    action_second = raw_data['action_second']
    future_stoploss = raw_data['strategy_future_contract_stoploss']
    future_target = raw_data['strategy_future_contract_target']
    option_stoploss = raw_data['strategy_option_contract_stoploss']
    option_target = raw_data['strategy_option_contract_target']

    contract_month = contract.strftime('%b')
    contract_year = contract.strftime('%Y')
    mysql_time_stamp = dt.datetime.now()

    sql_obj.add_alert_data(row_id, script_name, contract_month, contract_year, close, openx, high, low, volume, strat, strategy_timeframe_number, strategy_timeframe_unit,
                           action_first, action_second, future_stoploss, future_target, option_stoploss, option_target, 'Paper', mysql_time_stamp,
                           raw_data, alert_time, date, time_hour, time_min, time_seconds)

    curr_fut_expiry = zerodha_obj.get_current_futures_expiry(script_name)
    curr_opt_expiry = zerodha_obj.get_current_options_expiry(script_name)
    future_days = (curr_fut_expiry - dt.datetime.now()).days
    option_days = (curr_opt_expiry - dt.datetime.now()).days

    mysql_time_stamp = dt.datetime.now()

    sql_obj.add_tv_fno_data(row_id, curr_fut_expiry, curr_opt_expiry, future_days, option_days, mysql_time_stamp, raw_data, alert_time, date, time_hour, time_min, time_seconds)

    trade_temp_unique_key = uuid.uuid4()
    trade_insert_timestamp = dt.datetime.now()
    trade_insert_date = trade_insert_timestamp.date()
    o_strike_price = round(close / 100)*100
    inst_type = 'PE'
    if action_first.lower() == 'buy':
        inst_type = 'CE'

    t_symbol = zerodha_obj.get_option_based_on_expiry_strike(script_name, o_strike_price, curr_opt_expiry.strftime('%Y-%m-%d'), inst_type)
    ltp = zerodha_obj.get_ltp('NFO', t_symbol)

    sql_obj.add_trade_enter_log(trade_temp_unique_key, row_id, trade_insert_timestamp, trade_insert_date, close, alert_time, raw_data['contract'], future_days, curr_opt_expiry, option_days, o_strike_price, action_first, ltp, inst_type, 'executed')
    return "Done"



if __name__ == "__main__":
    api_key = 'qtwtwxrll4p0vjpe'
    api_secret = 'pawhue0joq5zc3ssa7ctyosauaans7er'
    user_id = 'WB6531'
    password = 'Mastermind@123'
    fa = '250990'
    zerodha_obj = FetchKiteData(api_key, api_secret, user_id, password, fa)
    # websocket_obj = KiteWebSocket()
    # websocket_obj.fkd = zerodha_obj
    # _thread.start_new_thread(self.kws.start, ())
    sql_obj = SqlConnector()
    app.run(host="0.0.0.0", port=80)
