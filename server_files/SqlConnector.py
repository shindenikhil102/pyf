import sys
import mysql.connector

from server_files.Logger import Logger


class SqlConnector:
    def __init__(self, host='15.206.106.151', username='nikhil', password='nikhil', database='tradingview_alert'):
        Logger.pprint('Creating a MySQL connection')

        try:
            self.mydb = mysql.connector.connect(
                host=host,
                user=username,
                password=password
            )

            if self.mydb:
                Logger.pprint('Connection successfully established')
            else:
                raise ConnectionError

            curr = self.mydb.cursor()
            curr.execute('SHOW DATABASES')
            dbs = [x[0] for x in curr]
            if database not in dbs:
                curr.execute(f'CREATE DATABASE {database}')

            curr.execute(f'USE {database}')
            curr.execute('SHOW TABLES')
            tables = [x[0] for x in curr]
            if 'tradingview_alert_raw_data' not in tables:
                curr.execute('''CREATE TABLE tradingview_alert_raw_data(
                    id INT NOT NULL AUTO_INCREMENT,
                    mysql_time_stamp TIMESTAMP NOT NULL,
                    raw_data TEXT NOT NULL,
                    alert_time TIMESTAMP NOT NULL,
                    date DATE NOT NULL,
                    time_hour INT NOT NULL,
                    time_min INT NOT NULL,
                    time_seconds INT NOT NULL,
                    PRIMARY KEY (id)
                )''')

            if 'tradingview_alert_data' not in tables:
                curr.execute('''CREATE TABLE tradingview_alert_data(
                    id INT NOT NULL AUTO_INCREMENT,
                    raw_data_id INT NOT NULL,
                    script_name VARCHAR(30) NOT NULL,
                    contract_month VARCHAR(10) NOT NULL,
                    contract_year INT NOT NULL,
                    current_price FLOAT NOT NULL,
                    current_candle_open FLOAT NOT NULL,
                    current_candle_high FLOAT NOT NULL,
                    current_candle_low FLOAT NOT NULL,
                    current_candle_volume INT NOT NULL,
                    str_name VARCHAR(20) NOT NULL,
                    str_timeframe_number INT NOT NULL,
                    str_timeframe_unit VARCHAR(10) NOT NULL,
                    action_enter VARCHAR(20) NOT NULL,
                    action_exit VARCHAR(20) NOT NULL,
                    str_fut_cont_sl INT NOT NULL,
                    str_fut_cont_tar INT NOT NULL,
                    str_opt_cont_sl INT NOT NULL,
                    str_opt_cont_tar INT NOT NULL,
                    trading_type VARCHAR(20) NOT NULL,
                    mysql_time_stamp TIMESTAMP NOT NULL,
                    raw_data TEXT NOT NULL,
                    alert_time TIMESTAMP NOT NULL,
                    date DATE NOT NULL,
                    time_hour INT NOT NULL,
                    time_min INT NOT NULL,
                    time_seconds INT NOT NULL,
                    PRIMARY KEY (id)
                )''')

            if 'tradingview_future_and_option_data' not in tables:
                curr.execute('''CREATE TABLE tradingview_future_and_option_data(
                    id INT NOT NULL AUTO_INCREMENT,
                    raw_data_id INT NOT NULL,
                    future_current_contract_end_date DATE NOT NULL,
                    option_current_contract_end_date DATE NOT NULL,
                    day_require_for_option_weekly_expiry INT NOT NULL,
                    day_require_for_future_monthly_expiry INT NOT NULL,
                    mysql_time_stamp TIMESTAMP NOT NULL,
                    raw_data TEXT NOT NULL,
                    alert_time TIMESTAMP NOT NULL,
                    date DATE NOT NULL,
                    time_hour INT NOT NULL,
                    time_min INT NOT NULL,
                    time_seconds INT NOT NULL,
                    PRIMARY KEY (id)
                )''')

            if 'trade_enter_log' not in tables:
                curr.execute('''CREATE TABLE `trade_enter_log` (
                    `trade_id` int(15) NOT NULL AUTO_INCREMENT,
                    `trade_temp_unique_key` varchar(100) NOT NULL,
                    `raw_data_id` int(15) NOT NULL,
                    `trade_insert_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    `trade_insert_date` date NOT NULL,
                    `future_price_in_raw_data` float NOT NULL,
                    `sin_comming_timestamp` timestamp NOT NULL,
                    `fut_current_contract` varchar(7) NOT NULL,
                    `future_current_contrac_expiry_remaning_days` int(3) NOT NULL,
                    `o_current_contract` date NOT NULL,
                    `o_cu_con_exp_rem_days` int(3) NOT NULL,
                    `o_strike_price` float NOT NULL,
                    `o_enter_action` varchar(5) NOT NULL,
                    `o_enter_price` int(7) NOT NULL,
                    `o_ce_or_pe` varchar(2) NOT NULL,
                    `o_orader_status` varchar(8) NOT NULL,
                    PRIMARY KEY (`trade_id`))''')

            curr.close()
        except Exception as e:
            Logger.exception(e)
            Logger.pprint(
                'Something went wrong while creating a MySQL connection. Exiting system.')
            sys.exit()

    def add_raw_alert(self, mysql_time_stamp, raw_data, alert_time, date, time_hour, time_min, time_seconds):
        try:
            curr = self.mydb.cursor()
            q = f'''INSERT INTO  tradingview_alert_raw_data (mysql_time_stamp, raw_data, alert_time, date, time_hour, time_min, time_seconds) 
                VALUES ('{mysql_time_stamp}', "{raw_data}", '{alert_time}', '{date}', {time_hour}, {time_min}, {time_seconds})'''
            curr.execute(q)
            self.mydb.commit()
            row_id = curr.lastrowid
            curr.close()
            return row_id
        except Exception as e:
            Logger.exception(e)
            Logger.pprint('Something went wrong while adding raw data to SQL')

    def add_alert_data(self, row_id, script_name, contract_month, contract_year, close, openx, high, low, volume, strat, strategy_timeframe_number, strategy_timeframe_unit,
                       action_first, action_second, future_stoploss, future_target, option_stoploss, option_target, trading_type, mysql_time_stamp,
                       raw_data, alert_time, date, time_hour, time_min, time_seconds):
        try:
            curr = self.mydb.cursor()
            q = f'''INSERT INTO `tradingview_alert_data` (`raw_data_id`, `script_name`, `contract_month`, `contract_year`, `current_price`, `current_candle_open`, `current_candle_high`, `current_candle_low`, `current_candle_volume`, `str_name`, `str_timeframe_number`, `str_timeframe_unit`, `action_enter`, `action_exit`, `str_fut_cont_sl`, `str_fut_cont_tar`, `str_opt_cont_sl`, `str_opt_cont_tar`, `trading_type`, `mysql_time_stamp`, `raw_data`, `alert_time`, `date`, `time_hour`, `time_min`, `time_seconds`) VALUES
                    ({row_id}, '{script_name}', '{contract_month}', {contract_year}, {close}, {openx}, {high}, {low}, {volume}, '{strat}', {strategy_timeframe_number}, '{strategy_timeframe_unit}', '{action_first}', '{action_second}', {future_stoploss}, {future_target}, {option_stoploss}, {option_target}, '{trading_type}', '{mysql_time_stamp}', "{raw_data}", '{alert_time}', '{date}', {time_hour}, {time_min}, {time_seconds})'''
            curr.execute(q)
            self.mydb.commit()
        except Exception as e:
            Logger.exception(e)
            Logger.pprint('Something went wrong while adding raw data to SQL')

    def add_tv_fno_data(self, row_id, curr_fut_expiry, curr_opt_expiry, future_days, option_days, mysql_time_stamp, raw_data, alert_time, date, time_hour, time_min, time_seconds):
        try:
            curr = self.mydb.cursor()
            q = f'''INSERT INTO `tradingview_future_and_option_data` (`raw_data_id`, `future_current_contract_end_date`, `option_current_contract_end_date`, `day_require_for_option_weekly_expiry`, `day_require_for_future_monthly_expiry`, `mysql_time_stamp`, `raw_data`, `alert_time`, `date`, `time_hour`, `time_min`, `time_seconds`) VALUES
                ({row_id}, '{curr_fut_expiry}', '{curr_opt_expiry}', {future_days}, {option_days}, '{mysql_time_stamp}', "{raw_data}", '{alert_time}', '{date}', {time_hour}, {time_min}, {time_seconds});'''
            curr.execute(q)
            self.mydb.commit()
        except Exception as e:
            Logger.exception(e)
            Logger.pprint('Something went wrong while adding raw data to SQL')

    def add_trade_enter_log(self, trade_temp_unique_key, raw_data_id, trade_insert_timestamp, trade_insert_date, future_price_in_raw_data, sin_comming_timestamp, fut_current_contract, future_current_contrac_expiry_remaning_days, o_current_contract, o_cu_con_exp_rem_days, o_strike_price, o_enter_action, o_enter_price, o_ce_or_pe, o_orader_status):
        try:
            curr = self.mydb.cursor()
            q = f'''INSERT INTO `trade_enter_log` (`trade_temp_unique_key`, `raw_data_id`, `trade_insert_timestamp`, `trade_insert_date`, `future_price_in_raw_data`, `sin_comming_timestamp`, `fut_current_contract`, `future_current_contrac_expiry_remaning_days`, `o_current_contract`, `o_cu_con_exp_rem_days`, `o_strike_price`, `o_enter_action`, `o_enter_price`, `o_ce_or_pe`, `o_orader_status`) VALUES
                ('{trade_temp_unique_key}', {raw_data_id}, '{trade_insert_timestamp}', '{trade_insert_date}', {future_price_in_raw_data}, '{sin_comming_timestamp}', '{fut_current_contract}', {future_current_contrac_expiry_remaning_days}, '{o_current_contract}', {o_cu_con_exp_rem_days}, {o_strike_price}, '{o_enter_action}', {o_enter_price}, '{o_ce_or_pe}', '{o_orader_status}');'''
            curr.execute(q)
            self.mydb.commit()
        except Exception as e:
            Logger.exception(e)
            Logger.pprint('Something went wrong while adding raw data to SQL')
