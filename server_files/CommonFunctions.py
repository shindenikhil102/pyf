import datetime
import time

from server_files.Logger import Logger

class CommonFunctions:

    # to round of price for entry, sl, target
    @staticmethod
    def price_myround(x, prec=2, base=.05):
        return round(base * round(float(x)/base),prec)

    # wait for any specific time
    @staticmethod
    def wait_for_time(_time, print_time=False):
        Logger.pprint(f'Waiting for time: { _time}')
        while datetime.datetime.now() <= _time:
            if print_time:
                print(datetime.datetime.now().strftime('%H:%M:%S'))
            else:
                pass
            time.sleep(1)

    @staticmethod
    def candle_close_timings(candle):
        """

        :param candle:
        :return:
        """

        _time = datetime.datetime(1, 1, 1, 9, 15)
        data = []
        while True:
            _time = _time + datetime.timedelta(minutes=candle)
            if _time.hour >= 16:
                break
            elif _time.hour == 15 and _time.minute > 30:
                break
            else:
                data.append(_time.strftime('%H:%M'))

        return data
