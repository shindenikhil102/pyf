import os, logging
from datetime import datetime
path = os.path.join(os.getcwd(), 'Log')
if not os.path.exists(path):
    os.mkdir(path)
logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',
                    filename=os.path.join(path, 'logs_' + datetime.today().strftime('%d_%m_%Y %H%M%S') + '.log'),
                    level=logging.INFO, datefmt='%H:%M:%S')

class Logger:
    @staticmethod
    def pprint(*messages):
        final_message = ''
        for message in messages:
            if isinstance(message, int) or isinstance(message, float):
                final_message = final_message + ' ' + str(message)
            elif isinstance(message, list):
                final_message = final_message + ' ' + ', '.join(str(x) for x in message)
            elif isinstance(message, str):
                final_message = final_message + ' ' + message
        print(datetime.now().strftime('%H:%M:%S   ') + final_message)
        logging.info(final_message)

    @staticmethod
    def exception(e):
        logging.exception(e)