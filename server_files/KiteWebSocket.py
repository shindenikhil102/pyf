from kiteconnect import KiteTicker
import _thread
from server_files.StockData import StockData as sd
import time


class KiteWebSocket:
    instrument_dict = {}
    thread_count = 0

    def __init__(self):
        self.kws = KiteTicker(sd.api_key, sd.access_token, reconnect_max_tries=300)
        self.fkd = None
        self.ws = None

    def check_entry(self, thread_name, ticks, ws):
        for _tick in ticks:
            instrument_token = int(_tick['instrument_token'])

            if instrument_token in sd.instrument_list:
                symbol = KiteWebSocket.instrument_dict[instrument_token]
                if symbol in sd.symbol_instance_dict:
                    instance = sd.symbol_instance_dict[symbol]
                    ltp = float(_tick['last_price'])

                    if instance.ltp is None:
                        sd.pprint(symbol, 'Feed started, LTP:', ltp)
                    instance.ltp = ltp
                    if 'depth' in _tick:
                        bid = _tick['depth']['buy'][0]['price']
                        ask = _tick['depth']['sell'][0]['price']
                        instance.bid = bid
                        instance.ask = ask

    def on_ticks(self, ws, ticks):
        thread_name = "Thread-" + str(KiteWebSocket.thread_count + 1)
        try:
            _thread.start_new_thread(self.check_entry, (thread_name, ticks, ws))
        except Exception as e:
            sd.exception(e)
            raise

    def on_connect(self, ws, response):
        try:
            ws.subscribe(sd.instrument_list)
            ws.set_mode(ws.MODE_FULL, sd.instrument_list)
            self.ws = ws
        except Exception as e:
            sd.exception(e)

    def on_order_update(self, ws, _temp):
        order_id = str(_temp['order_id'])
        status = _temp['status']

        if order_id in sd.order_id_list:
            if sd.order_status_dict[order_id]['status'] not in ['COMPLETE', 'REJECTED', 'CANCELLED']:
                sd.pprint('OU: ', order_id, ' status:', _temp['status'], 'Avg price:', _temp['average_price'], 'Time:',
                          _temp['exchange_timestamp'])
                if status in ['COMPLETE', 'REJECTED', 'CANCELLED', 'TRIGGER PENDING', 'OPEN']:
                    data = {'status': status, 'price': None, 'time': None}
                    if status == 'COMPLETE':
                        data = {'status': status, 'price': _temp['average_price'], 'time': _temp['exchange_timestamp']}
                    if status in ['COMPLETE', 'REJECTED', 'CANCELLED']:
                        if order_id in sd.order_id_pending_update_list:
                            sd.order_id_pending_update_list.remove(order_id)
                    sd.order_status_dict[order_id] = data

    def start(self):
        sd.pprint('Subscribing to all the symbols...')
        _list = []
        for symbol in sd.symbol_list:
            instance = sd.symbol_instance_dict[symbol]
            _list.append(int(self.fkd.get_instrument_token(exchange=instance.exchange, symbol=symbol)))
        sd.instrument_list = _list
        sd.pprint('Starting web socket...')
        self.kws.on_ticks = self.on_ticks
        self.kws.on_connect = self.on_connect
        self.kws.on_order_update = self.on_order_update
        self.kws.connect(threaded=True)
        while True:
            time.sleep(3)
            if sd.force_close:
                break
