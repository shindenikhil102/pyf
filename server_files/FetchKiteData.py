from kiteconnect import KiteConnect
import time
from server_files.ZerodhaGenerateAccessToken import ZerodhaGenerateAccessToken
from datetime import datetime, timedelta, date
# from server_files.StockData import StockData as sd
from server_files.Logger import Logger
from server_files.CommonFunctions import CommonFunctions as cm
import pandas as pd
import _thread
import sys
import os
import re
import pytz

IST = pytz.timezone('Asia/Kolkata')

class FetchKiteData:
    def __init__(self, api_key, api_secret, client_id, password, pin):
        if datetime.now(IST).strftime('%H:%M:%S') <= '07:30:00':
            Logger.pprint('Waiting for 07:30:00')
            cm.wait_for_time('07:30:00')
        Logger.pprint("Today is", datetime.today().strftime('%d-%m-%Y'))
        Logger.pprint('Working directory:', os.getcwd())
        token = ZerodhaGenerateAccessToken(api_key, api_secret, client_id, password, pin)
        self.instrument_dict = {}  # {instrument token : symbol}  # this is used for web sockets
        self.access_token = token.return_access_token()
        for i in range(0, 100):
            try:
                self.kite = KiteConnect(api_key=api_key)
                self.kite.set_access_token(self.access_token)
                break
            except Exception as e:
                Logger.exception(e)
                time.sleep(1)
                continue

        try:
            Logger.pprint('Checking if token is valid...')
            self.kite.profile()
            Logger.pprint('Token generated successfully...')
        except Exception as e:
            Logger.exception(e)
            Logger.pprint('Token not generated successfully... please start again...')
            sys.exit()

        for i in range(0, 100):
            try:
                Logger.pprint("Trying to fetch instrument tokens...")
                self.a = self.kite.instruments()
                self.df = pd.DataFrame(self.a)
                Logger.pprint("Instrument tokens fetched...")
                break
            except Exception as e:
                Logger.exception(e)
                time.sleep(1)
                continue

        for _a in self.a:
            self.instrument_dict[int(_a['instrument_token'])] = _a['tradingsymbol']

    def instrumentLookup(self, symbol):
        try:
            return self.df[self.df.tradingsymbol==symbol].instrument_token.values[0]
        except:
            return -1

    def get_current_futures_expiry(self, script):
        fut_df = self.df[(self.df['name'] == script.upper()) & (self.df['segment'] == 'NFO-FUT')]
        expiry = pd.to_datetime(fut_df['expiry']).min()
        
        return expiry
    
    def get_current_options_expiry(self, script):
        opt_df = self.df[(self.df['name'] == script.upper()) & (self.df['segment'] == 'NFO-OPT')]
        expiry = pd.to_datetime(opt_df['expiry']).min()

        return expiry

    def get_ohlc(self, ticker, interval, duration):
        instrument = self.instrumentLookup(ticker)
        data = pd.DataFrame(self.kite.historical_data(instrument, date.today()-timedelta(duration), date.today(),interval))
        return data

    def get_ltp(self, exchange, ticker):
        instrument_token = f'{exchange}:{ticker}'
        for i in range(0, 100):
            try:
                ltp = self.kite.ltp([instrument_token])
                break
            except Exception as e:
                Logger.exception(e)
                time.sleep(1)
                continue
        if ltp:
            for instrument_token in ltp:
                return ltp[instrument_token]['last_price']

    @staticmethod
    def extract_symbol(symbol):
        return re.match(r"([-&a-zA-Z]+)([0-9]+)", symbol).group(1)


    def get_option_based_on_expiry_strike(self, symbol, strike, expiry, instrument_type):
        """

        :param symbol:
        :param strike:
        :param expiry:
        :param instrument_type: CE/PE
        :return:
        """
        strike = float(strike)
        pd.set_option('mode.chained_assignment', None)
        df = self.df
        df1 = df[df["exchange"] == self.kite.EXCHANGE_NFO]
        df1['symbol'] = df1['tradingsymbol'].apply(lambda x: FetchKiteData.extract_symbol(x))
        df1 = df1[df1['symbol'] == symbol.upper()]
        df1['dateonly'] = df1['expiry'].apply(lambda x: x.strftime("%Y-%m-%d"))
        df1 = df1[df1['dateonly'] == expiry]
        df1 = df1[df1['strike'] == strike]
        df1 = df1[df1['instrument_type'] == instrument_type.upper()]
        df1.reset_index(inplace=True, drop=True)
        pd.set_option('mode.chained_assignment', 'warn')
        return df1.at[0, 'tradingsymbol']
