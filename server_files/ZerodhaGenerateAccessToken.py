from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from kiteconnect import KiteConnect
import re, time
import os
import sys
from server_files.Logger import Logger
from datetime import datetime
import chromedriver_autoinstaller

chromedriver_autoinstaller.install()


class ZerodhaGenerateAccessToken:
    headless = True

    def __init__(self, api_key, api_secret, client_id, password, pin):
        self.api_key = api_key
        self.api_secret = api_secret
        self.client_id = client_id
        self.password = password
        self.fa = pin
        if len(pin) != 6:
            Logger.pprint(
                'Pin length should be 6. Also, it should not start with a 0 on the left side. '
                'Please change and start again...')
            sys.exit()

        self.kite = KiteConnect(api_key=api_key)
        # self.get_access_token()
        # self.access_token_path = os.path.join(os.getcwd(), 'access_token.txt')
        Logger.pprint('Login into kite connect...')
        self.get_access_token()

    def get_access_token(self):
        request_token = self.get_request_token()
        data = None
        for i in range(0, 100):
            try:
                data = self.kite.generate_session(request_token, self.api_secret)
                break
            except Exception as e:
                Logger.exception(e)
                time.sleep(1)
                continue
        if data is not None:
            Logger.pprint("Access Token generated successfully...")
            Logger.pprint('Access token: ', data['access_token'])
            Logger.pprint('Login done...')
            self.access_token = data['access_token']

    def get_request_token(self):
        request_token = None
        login_url = self.kite.login_url()
        options = Options()
        options.add_argument("--headless")
        if ZerodhaGenerateAccessToken.headless:
            browser = webdriver.Chrome(chrome_options=options)
        else:
            browser = webdriver.Chrome()
        try:
            browser.get(login_url)
            # time.sleep(5)
            for i in range(0, 2):
                try:
                    credentials = browser.find_elements_by_tag_name("input")
                    user_name = credentials[0]
                    user_pass = credentials[1]
                    user_name.send_keys(self.client_id)
                    user_pass.send_keys(self.password)
                    browser.find_element_by_class_name("button-orange").click()
                    time.sleep(2)
                    pin = browser.find_elements_by_tag_name('input')
                    pin[0].send_keys(self.fa)
                    for i in range(0, 5):
                        try:
                            browser.find_element_by_class_name("button-orange").click()
                            time.sleep(2)
                            break
                        except Exception as e:
                            pass
                    try:
                        browser.find_element_by_xpath('/html/body/div[1]/div/div[1]/div/div/div[3]/button').click()
                        break
                    except:
                        pass
                    time.sleep(5)
                    request_token = re.findall(r'(?<=request_token=)(\w*)', str(browser.current_url))[0]
                except Exception as e:
                    Logger.exception(e)
                    time.sleep(1)
                    continue
                break
        finally:
            browser.quit()
        return request_token

    def return_access_token(self):
        return self.access_token


if __name__ == '__main__':
    ZerodhaGenerateAccessToken()

